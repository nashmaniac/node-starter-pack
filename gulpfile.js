/**
 * Created by asci on 1/26/16.
 */
var gulp=require('gulp');
var gulp_config=require('./gulp_config.json');

gulp.task('transfer-html',function(){
    return gulp.src(gulp_config.source.html,{base:gulp_config.base.source})
        .pipe(gulp.dest(gulp_config.destination.html));
});
gulp.task('transfer-js',function(){
    return gulp.src(gulp_config.source.scripts,{base:gulp_config.base.source})
        .pipe(gulp.dest(gulp_config.destination.scripts));
});

gulp.task('transfer-css',function(){
    return gulp.src(gulp_config.source.styles,{base:gulp_config.base.source})
        .pipe(gulp.dest(gulp_config.destination.styles));
});
gulp.task('watch',function(){
    gulp.watch(gulp_config.source.html,['transfer-html']);
    gulp.watch(gulp_config.source.scripts,['transfer-js']);
    gulp.watch(gulp_config.source.styles,['transfer-css']);
});
gulp.task('default',['transfer-html','transfer-js','transfer-css','watch']);