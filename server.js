/**
 * Created by asci on 1/26/16.
 */
var server_config=require('./server_config');

var express=require('express');
var morgan=require('morgan');
var bodyParser=require('body-parser');

var app=express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

//creating morgan date_type token
morgan.token('date_type',function(){
    return Date().toLocaleString();
});
//custom logger format
morgan.format('custom', '[:date_type] :method :url :status :res[content-length] - :response-time ms')

app.use(morgan('custom'));

//using the express static folder
app.use(express.static(server_config.staticFolder));

app.get('*',function(req,res){
    res.sendFile(server_config.staticFolder+'views/index.html');
});
app.listen(server_config.port,function(err){
    if(err){
        console.log(err);
    }
    console.log('Server is running at port '+server_config.port);
});