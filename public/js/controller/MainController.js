/**
 * Created by asci on 1/27/16.
 */
(function(){
    'use strict';
    var mainCtrl=angular.module('MainController',[]);
    mainCtrl.controller('MainControl',function(){

    });
    mainCtrl.controller('loginController',function(){
        var vm=this;
        vm.submitLogin=function(){
            console.log(vm.username);
            console.log(vm.password);
        }
    });
    mainCtrl.controller('signupController',function(ngNotify){
        var vm=this;
        vm.submitSignup=function(){
            if(vm.password==""||vm.password==undefined){
                ngNotify.set("Password can not be empty",{
                    type:'error'
                });
                return false;
            }
            if(vm.password!=vm.confirm_password){
                ngNotify.set("Password don't match",{
                    type:'error'
                });
                vm.password="";
                vm.confirm_password="";
            }
        };
    });
})();